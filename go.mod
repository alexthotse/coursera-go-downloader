module coursera_dl_golang

go 1.21.5

require (
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/kevinburke/ssh_config v0.0.0-20180725182655-28f7f970d295 // indirectly used by keyring
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20211201170734-937a96ef9799 // indirectly used by keyring
	golang.org/x/sys v0.0.0-20211202233343-2af9fde63516 // indirectly used by keyring
	golang.org/x/text v0.3.10
//	golang.org/x/tools v0.1.8 // indirectly used by keyring
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirectly used by keyring
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
//	gopkg.in/yaml.v2 v2.2.8
)

replace (
	golang.org/x/crypto v0.0.0-20211207161147-7901fa4af8b2 => golang.org/x/crypto v0.0.0-20211201174958-181b0c317268
)

// go.sum
//...

// go test command
// go test ./...
