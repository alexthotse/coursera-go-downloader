package main

import (
	"encoding/json"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"math/rand"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"sort"
	"strings"
	"time"
)

const COURSERA_URL = "https://www.coursera.org"

var WINDOWS_UNC_PREFIX = `\\?\`

func CreateM3UPlaylist(sectionDir string) {
	pathToReturn, err := os.Getwd()
	if err != nil {
		log.Fatalf("Error getting current working directory: %s", err)
	}

	err = filepath.Walk(sectionDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("Error accessing path %s: %s", path, err)
			return nil
		}

		if info.IsDir() {
			os.Chdir(path)
			globbedVideos, _ := filepath.Glob("*.mp4")
			sort.Strings(globbedVideos)
			m3uName := filepath.Base(path) + ".m3u"

			if len(globbedVideos) > 0 {
				m3uPath := filepath.Join(path, m3uName)
				if err := writeM3UPlaylist(m3uPath, globbedVideos); err != nil {
					log.Printf("Error creating M3U playlist %s: %s", m3uPath, err)
				}
			}

			os.Chdir(pathToReturn)
		}

		return nil
	})

	if err != nil {
		log.Fatalf("Error walking through directory: %s", err)
	}

	os.Chdir(pathToReturn)
}

func writeM3UPlaylist(m3uPath string, videos []string) error {
	content := strings.Join(videos, "\n") + "\n"
	return ioutil.WriteFile(m3uPath, []byte(content), os.ModePerm)
}

func SpitJSON(obj interface{}, filename string) {
	jsonData, err := json.MarshalIndent(obj, "", "    ")
	if err != nil {
		log.Fatalf("Error marshaling JSON data: %s", err)
	}

	err = ioutil.WriteFile(filename, jsonData, os.ModePerm)
	if err != nil {
		log.Fatalf("Error writing JSON file: %s", err)
	}
}

func SlurpJSON(filename string, obj interface{}) {
	fileData, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Error reading JSON file: %s", err)
	}

	err = json.Unmarshal(fileData, obj)
	if err != nil {
		log.Fatalf("Error unmarshaling JSON data: %s", err)
	}
}

func IsDebugRun() bool {
	return log.Prefix() == "DEBUG"
}

func RandomString(length int) string {
	const validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	result := make([]byte, length)
	for i := range result {
		result[i] = validChars[rand.Intn(len(validChars))]
	}
	return string(result)
}

func UnescapeHTML(s string) string {
	return html.UnescapeString(url.QueryEscape(s))
}

func CleanFilename(s string, minimalChange bool) string {
	s = UnescapeHTML(s)
	s = strings.ReplaceAll(s, ":", "-")
	s = strings.ReplaceAll(s, "/", "-")
	s = strings.ReplaceAll(s, "<", "-")
	s = strings.ReplaceAll(s, ">", "-")
	s = strings.ReplaceAll(s, `"`, "-")
	s = strings.ReplaceAll(s, `\`, "-")
	s = strings.ReplaceAll(s, "|", "-")
	s = strings.ReplaceAll(s, "?", "-")
	s = strings.ReplaceAll(s, "*", "-")
	s = strings.ReplaceAll(s, "\x00", "-")
	s = strings.ReplaceAll(s, "\n", " ")

	s = strings.TrimRight(s, " .")

	if minimalChange {
		return s
	}

	s = strings.ReplaceAll(s, "(", "")
	s = strings.ReplaceAll(s, ")", "")
	s = strings.TrimRight(s, ".")
	s = strings.TrimSpace(s)
	s = strings.ReplaceAll(s, " ", "_")

	const validChars = "-_.()%s%s"
	var cleaned string
	for _, char := range s {
		if strings.ContainsAny(string(char), validChars) {
			cleaned += string(char)
		}
	}

	return cleaned
}

func NormalizePath(path string) string {
	if runtime.GOOS != "windows" {
		return path
	}

	if strings.HasPrefix(path, WINDOWS_UNC_PREFIX) {
		return path
	}

	return WINDOWS_UNC_PREFIX + filepath.FromSlash(filepath.Join(filepath.Abs(path)))
}

func GetAnchorFormat(a string) string {
	re := regexp.MustCompile(`(?:\.|format=)(\w+)(?:\?.*)?$`)
	match := re.FindStringSubmatch(a)
	if len(match) > 1 {
		return match[1]
	}
	return ""
}

func MkdirP(path string, mode os.FileMode) {
	err := os.MkdirAll(path, mode)
	if err != nil && !os.IsExist(err) {
		log.Fatalf("Error creating directory hierarchy: %s", err)
	}
}

func CleanURL(url string) string {
	parsed, err := url.Parse(strings.TrimSpace(url))
	if err != nil {
		log.Fatalf("Error parsing URL: %s", err)
	}

	parsed.Fragment = ""
	parsed.RawQuery = ""
	parsed.RawPath = ""
	parsed.Path = strings.TrimSuffix(parsed.Path, "/")

	return parsed.String()
}

func FixURL(url string) string {
	url = strings.TrimSpace(url)
	if url == "" {
		return url
	}

	parsed, err := url.Parse(url)
	if err != nil {
		log.Fatalf("Error parsing URL: %s", err)
	}

	if parsed.Scheme == "" {
		url = "http://" + url
	}

	return url
}

func IsCourseComplete(lastUpdate int64) bool {
	if lastUpdate < 0 {
		return false
	}

	delta := time.Now().Unix() - lastUpdate
	maxDelta := int64(30 * 24 * 3600) // 30 days in seconds

	return delta > maxDelta
}

func TotalSeconds(td time.Duration) int64 {
	return int64(td.Seconds())
}

func MakeCourseraAbsoluteURL(url string) string {
	parsedURL, err := url.Parse(url)
	if err != nil {
		log.Fatalf("Error parsing URL: %s", err)
	}

	if parsedURL.Host == "" {
		return urljoin(COURSERA_URL, url)
	}

	return url
}

func ExtendSupplementLinks(destination, source map[string][]string) {
	for key, value := range source {
		if _, exists := destination[key]; !exists {
			destination[key] = value
		} else {
			destination[key] = append(destination[key], value...)
		}
	}
}

func PrintSSLErrorMessage(exception error) {
	message := fmt.Sprintf(`
#####################################################################
# ATTENTION! PLEASE READ THIS!
#
# The following error has just occurred:
# %s %s
#
# Please read instructions on how to fix this error here:
# https://github.com/coursera-dl/coursera-dl#sslerror-errno-1-_sslc504-error14094410ssl-r
`)
}
