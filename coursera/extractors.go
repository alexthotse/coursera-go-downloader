package coursera

import (
	"encoding/json"
	"fmt"
	"log"
)

// PlatformExtractor is an interface for platform-specific extractors.
type PlatformExtractor interface {
	GetModules() []Module
}

// CourseraExtractor is an implementation of PlatformExtractor for Coursera.
type CourseraExtractor struct {
	Session *Session // Assuming a Session type is available
}

// Module represents a course module.
type Module struct {
	Slug    string
	Lessons []Lesson
}

// Lesson represents a module lesson.
type Lesson struct {
	Slug     string
	Lectures []Lecture
}

// Lecture represents a lesson lecture.
type Lecture struct {
	Slug  string
	Links []Link
}

// Link represents a downloadable link.
type Link struct {
	Name string
	URL  string
}

// GetModules retrieves the course modules.
func (ce *CourseraExtractor) GetModules(
	className string,
	reverse, unrestrictedFilenames bool,
	subtitleLanguage, videoResolution string,
	downloadQuizzes bool,
	mathJaxCDNURL string,
	downloadNotebooks bool,
) ([]Module, error) {
	page, err := ce.getOnDemandSyllabus(className)
	if err != nil {
		return nil, err
	}

	errorOccurred, modules := ce.parseOnDemandSyllabus(
		className,
		page,
		reverse,
		unrestrictedFilenames,
		subtitleLanguage,
		videoResolution,
		downloadQuizzes,
		mathJaxCDNURL,
		downloadNotebooks,
	)

	if errorOccurred {
		return nil, err
	}

	return modules, nil
}

func (ce *CourseraExtractor) getOnDemandSyllabus(className string) (string, error) {
	url := fmt.Sprintf(OPENCOURSE_ONDEMAND_COURSE_MATERIALS_V2, className)
	page, err := ce.getSession().GetPage(url)
	if err != nil {
		log.Printf("Error downloading %s: %v", url, err)
		return "", err
	}

	log.Printf("Downloaded %s (%d bytes)", url, len(page))
	return page, nil
}

func (ce *CourseraExtractor) parseOnDemandSyllabus(
	courseName, page string,
	reverse, unrestrictedFilenames bool,
	subtitleLanguage, videoResolution string,
	downloadQuizzes bool,
	mathJaxCDNURL string,
	downloadNotebooks bool,
) (bool, []Module) {
	var dom map[string]interface{}
	if err := json.Unmarshal([]byte(page), &dom); err != nil {
		log.Printf("Error parsing JSON: %v", err)
		return true, nil
	}

	classID := dom["elements"].([]interface{})[0].(map[string]interface{})["id"].(string)

	log.Printf("Parsing syllabus of on-demand course (id=%s). This may take some time, please be patient ...", classID)
	var modules []Module

	jsonModules := dom["linked"].(map[string]interface{})["onDemandCourseMaterialItems.v2"]
	course := NewCourseraOnDemand(
		ce.getSession(),
		classID,
		courseName,
		unrestrictedFilenames,
		mathJaxCDNURL,
	)
	course.ObtainUserID()

	ondemandMaterialItems := NewOnDemandCourseMaterialItemsV1(ce.getSession(), courseName)

	if IsDebugRun() {
		SpitJSON(dom, fmt.Sprintf("%s-syllabus-raw.json", courseName))
		SpitJSON(jsonModules, fmt.Sprintf("%s-material-items-v2.json", courseName))
		SpitJSON(ondemandMaterialItems.GetItems(), fmt.Sprintf("%s-course-material-items.json", courseName))
	}

	errorOccurred := false

	allModules := NewModulesV1FromJSON(
		dom["linked"].(map[string]interface{})["onDemandCourseMaterialModules.v1"],
	)
	allLessons := NewLessonsV1FromJSON(
		dom["linked"].(map[string]interface{})["onDemandCourseMaterialLessons.v1"],
	)
	allItems := NewItemsV2FromJSON(
		dom["linked"].(map[string]interface{})["onDemandCourseMaterialItems.v2"],
	)

	for _, module := range allModules {
		log.Printf("Processing module  %s", module.Slug)
		var lessons []Lesson

		for _, section := range module.Children(allLessons) {
			log.Printf("Processing section     %s", section.Slug)
			var lectures []Lecture
			availableLectures := section.Children(allItems)

			// Certain modules may be empty-looking programming assignments
			// e.g. in data-structures, algorithms-on-graphs ondemand
			// courses
			if len(availableLectures) == 0 {
				lecture := ondemandMaterialItems.Get(section.ID)
				if lecture != nil {
					availableLectures = []*ItemV2{lecture}
				}
			}

			for _, lecture := range availableLectures {
				typeName := lecture.TypeName

				log.Printf("Processing lecture         %s (%s)", lecture.Slug, typeName)
				// Empty dictionary means there were no data
				// None means an error occurred
				links := []Link{}

				switch typeName {
				case "lecture":
					lectureVideoID := lecture.ID
					links = course.ExtractLinksFromLecture(
						classID,
						lectureVideoID)
				}
			}
		}
	}
	return false, nil
}
