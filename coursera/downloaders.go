package coursera

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

// Downloader interface defines the contract for downloaders.
type Downloader interface {
	Download(url, filename string, resume bool) bool
}

// ExternalDownloader is a base struct for external downloaders.
type ExternalDownloader struct {
	Bin                 string
	DownloaderArguments []string
}

// WgetDownloader is an implementation of the Downloader interface using wget.
type WgetDownloader struct {
	ExternalDownloader
}

// CurlDownloader is an implementation of the Downloader interface using curl.
type CurlDownloader struct {
	ExternalDownloader
}

// Aria2Downloader is an implementation of the Downloader interface using aria2.
type Aria2Downloader struct {
	ExternalDownloader
}

// AxelDownloader is an implementation of the Downloader interface using axel.
type AxelDownloader struct {
	ExternalDownloader
}

// NativeDownloader is an implementation of the Downloader interface using Go's native downloader.
type NativeDownloader struct {
	Session *http.Client
}

// NewExternalDownloader creates a new ExternalDownloader instance.
func NewExternalDownloader(bin string, downloaderArguments []string) *ExternalDownloader {
	return &ExternalDownloader{
		Bin:                 bin,
		DownloaderArguments: downloaderArguments,
	}
}

// NewWgetDownloader creates a new WgetDownloader instance.
func NewWgetDownloader() *WgetDownloader {
	return &WgetDownloader{ExternalDownloader: ExternalDownloader{Bin: "wget"}}
}

// NewCurlDownloader creates a new CurlDownloader instance.
func NewCurlDownloader() *CurlDownloader {
	return &CurlDownloader{ExternalDownloader: ExternalDownloader{Bin: "curl"}}
}

// NewAria2Downloader creates a new Aria2Downloader instance.
func NewAria2Downloader() *Aria2Downloader {
	return &Aria2Downloader{ExternalDownloader: ExternalDownloader{Bin: "aria2c"}}
}

// NewAxelDownloader creates a new AxelDownloader instance.
func NewAxelDownloader() *AxelDownloader {
	return &AxelDownloader{ExternalDownloader: ExternalDownloader{Bin: "axel"}}
}

// NewNativeDownloader creates a new NativeDownloader instance.
func NewNativeDownloader() *NativeDownloader {
	return &NativeDownloader{Session: &http.Client{}}
}

// EnableResume enables resume feature for external downloaders.
func (e *ExternalDownloader) EnableResume(command []string) {
	// Implement as needed for each downloader
}

// AddCookies adds the given cookie values to the command for external downloaders.
func (e *ExternalDownloader) AddCookies(command []string, cookieValues string) {
	// Implement as needed for each downloader
}

// CreateCommand creates a command to execute in a subprocess for external downloaders.
func (e *ExternalDownloader) CreateCommand(url, filename string) []string {
	// Implement as needed for each downloader
	return nil
}

// StartDownload starts the download process for external downloaders.
func (e *ExternalDownloader) StartDownload(url, filename string, resume bool) {
	command := e.CreateCommand(url, filename)
	command = append(command, e.DownloaderArguments...)
	e.PrepareCookies(command, url)
	if resume {
		e.EnableResume(command)
	}

	log.Printf("Executing %s: %s\n", e.Bin, command)
	err := executeCommand(command)
	if err != nil {
		log.Fatal(err)
	}
}

// PrepareCookies extracts cookies from the HTTP client's session and adds them to the command.
func (e *ExternalDownloader) PrepareCookies(command []string, url string) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}

	cookieValues := strings.Join(e.Session.Jar.Cookies(req.URL), "; ")
	if cookieValues != "" {
		e.AddCookies(command, cookieValues)
	}
}

// EnableResume enables the resume feature for wget.
func (w *WgetDownloader) EnableResume(command []string) {
	command = append(command, "-c")
}

// AddCookies adds the given cookie values to the command for wget.
func (w *WgetDownloader) AddCookies(command []string, cookieValues string) {
	command = append(command, "--header", "Cookie: "+cookieValues)
}

// CreateCommand creates a command to execute in a subprocess for wget.
func (w *WgetDownloader) CreateCommand(url, filename string) []string {
	return []string{w.Bin, url, "-O", filename, "--no-cookies", "--no-check-certificate"}
}

// EnableResume enables the resume feature for curl.
func (c *CurlDownloader) EnableResume(command []string) {
	command = append(command, "-C", "-")
}

// AddCookies adds the given cookie values to the command for curl.
func (c *CurlDownloader) AddCookies(command []string, cookieValues string) {
	command = append(command, "--cookie", cookieValues)
}

// CreateCommand creates a command to execute in a subprocess for curl.
func (c *CurlDownloader) CreateCommand(url, filename string) []string {
	return []string{c.Bin, url, "-k", "-#", "-L", "-o", filename}
}

// EnableResume enables the resume feature for aria2.
func (a *Aria2Downloader) EnableResume(command []string) {
	command = append(command, "-c")
}

// AddCookies adds the given cookie values to the command for aria2.
func (a *Aria2Downloader) AddCookies(command []string, cookieValues string) {
	command = append(command, "--header", "Cookie: "+cookieValues)
}

// CreateCommand creates a command to execute in a subprocess for aria2.
func (a *Aria2Downloader) CreateCommand(url, filename string) []string {
	return []string{a.Bin, url, "-o", filename, "--check-certificate=false", "--log-level=notice", "--max-connection-per-server=4", "--min-split-size=1M"}
}

// StartDownload starts the download process for NativeDownloader.
func (n *NativeDownloader) StartDownload(url, filename string, resume bool) {
	r, err := n.Session.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()

	var fileSize int64
	if resume {
		fileInfo, err := os.Stat(filename)
		if err != nil {
			log.Fatal(err)
		}
		fileSize = fileInfo.Size()
	}

	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	if resume {
		_, err := file.Seek(fileSize, 0)
		if err != nil {
			log.Fatal(err)
		}
	}

	_, err = io.Copy(file, r.Body)
	if err != nil {
		log.Fatal(err)
	}
}

// Download downloads the given URL to the given file.
func (n *NativeDownloader) Download(url, filename string, resume bool) bool {
	fmt.Printf("Downloading %s -> %s\n", url, filename)
	n.StartDownload(url)
}
