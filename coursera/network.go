package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func getReply(session *http.Client, urlStr string, post bool, data interface{}, headers map[string]string, quiet bool) (*http.Response, error) {
	method := "GET"
	if post {
		method = "POST"
	}

	request, err := http.NewRequest(method, urlStr, nil)
	if err != nil {
		return nil, err
	}

	// Add payload data
	if post && data != nil {
		request.Header.Set("Content-Type", "application/json") // Assuming JSON data
		// Convert data to JSON and set as request body
		// Note: Replace this part with your own implementation based on the data type
		// For simplicity, I'm assuming data is a JSON-serializable type
		requestData, err := json.Marshal(data)
		if err != nil {
			return nil, err
		}
		request.Body = ioutil.NopCloser(bytes.NewReader(requestData))
	}

	// Add headers
	for key, value := range headers {
		request.Header.Set(key, value)
	}

	reply, err := session.Do(request)
	if err != nil {
		return nil, err
	}

	if !quiet && reply.StatusCode != http.StatusOK {
		log.Printf("Error %d getting page %s", reply.StatusCode, urlStr)
		body, _ := ioutil.ReadAll(reply.Body)
		log.Printf("The server replied: %s", body)
		return nil, fmt.Errorf("HTTP error: %d", reply.StatusCode)
	}

	return reply, nil
}

func getPage(session *http.Client, urlStr string, jsonData bool, post bool, data interface{}, headers map[string]string, quiet bool, args map[string]string) (string, error) {
	urlStr = formatURL(urlStr, args)
	reply, err := getReply(session, urlStr, post, data, headers, quiet)
	if err != nil {
		return "", err
	}
	defer reply.Body.Close()

	responseBody, err := ioutil.ReadAll(reply.Body)
	if err != nil {
		return "", err
	}

	if jsonData {
		return string(responseBody), nil
	}
	return responseBody, nil
}

func getPageAndURL(session *http.Client, urlStr string) (string, string, error) {
	reply, err := getReply(session, urlStr, false, nil, nil, false)
	if err != nil {
		return "", "", err
	}
	defer reply.Body.Close()

	responseBody, err := ioutil.ReadAll(reply.Body)
	if err != nil {
		return "", "", err
	}

	return string(responseBody), reply.URL.String(), nil
}

func postPageAndReply(session *http.Client, urlStr string, data interface{}, headers map[string]string, args map[string]string) (string, *http.Response, error) {
	urlStr = formatURL(urlStr, args)
	reply, err := getReply(session, urlStr, true, data, headers, false)
	if err != nil {
		return "", nil, err
	}
	defer reply.Body.Close()

	responseBody, err := ioutil.ReadAll(reply.Body)
	if err != nil {
		return "", nil, err
	}

	return string(responseBody), reply, nil
}

// formatURL replaces the placeholders in the URL with values from args.
func formatURL(urlStr string, args map[string]string) string {
	for key, value := range args {
		urlStr = strings.Replace(urlStr, "{"+key+"}", value, -1)
	}
	return urlStr
}
