package main

import (
	"errors"
	"log"
	"sync"
)

// AbstractDownloader is an interface for download wrappers.
type AbstractDownloader interface {
	Download(callback func(url string, result interface{}), url string, args ...interface{}) interface{}
	Join()
}

// ConsecutiveDownloader is a class that calls the underlying file downloader sequentially.
type ConsecutiveDownloader struct {
	fileDownloader FileDownloader
}

// NewConsecutiveDownloader creates a new ConsecutiveDownloader.
func NewConsecutiveDownloader(fileDownloader FileDownloader) *ConsecutiveDownloader {
	return &ConsecutiveDownloader{fileDownloader: fileDownloader}
}

// Download calls the underlying file downloader in a sequential order.
func (cd *ConsecutiveDownloader) Download(callback func(url string, result interface{}), url string, args ...interface{}) interface{} {
	result, err := cd.downloadWrapper(url, args...)
	if err != nil {
		log.Printf("ConsecutiveDownloader: %s", err)
	}
	callback(url, result)
	return result
}

// Join does nothing for ConsecutiveDownloader.
func (cd *ConsecutiveDownloader) Join() {
	// Do nothing for ConsecutiveDownloader
}

func (cd *ConsecutiveDownloader) downloadWrapper(url string, args ...interface{}) (interface{}, error) {
	result, err := cd.fileDownloader.Download(url, args...)
	if err != nil {
		return nil, errors.New("ConsecutiveDownloader: " + err.Error())
	}
	return result, nil
}

// ParallelDownloader is a class that uses goroutines to run download requests in parallel.
type ParallelDownloader struct {
	fileDownloader FileDownloader
	processes      int
	wg             sync.WaitGroup
}

// NewParallelDownloader creates a new ParallelDownloader.
func NewParallelDownloader(fileDownloader FileDownloader, processes int) *ParallelDownloader {
	return &ParallelDownloader{
		fileDownloader: fileDownloader,
		processes:      processes,
	}
}

// Download calls the underlying file downloader in parallel.
func (pd *ParallelDownloader) Download(callback func(url string, result interface{}), url string, args ...interface{}) interface{} {
	pd.wg.Add(1)
	go pd.downloadAsync(callback, url, args...)
	return nil // Result will be sent through the callback
}

// Join waits for all goroutines to finish.
func (pd *ParallelDownloader) Join() {
	pd.wg.Wait()
}

func (pd *ParallelDownloader) downloadAsync(callback func(url string, result interface{}), url string, args ...interface{}) {
	defer pd.wg.Done()
	result, err := pd.downloadWrapper(url, args...)
	if err != nil {
		log.Printf("ParallelDownloader: %s", err)
	}
	callback(url, result)
}

func (pd *ParallelDownloader) downloadWrapper(url string, args ...interface{}) (interface{}, error) {
	result, err := pd.fileDownloader.Download(url, args...)
	if err != nil {
		return nil, errors.New("ParallelDownloader: " + err.Error())
	}
	return result, nil
}
