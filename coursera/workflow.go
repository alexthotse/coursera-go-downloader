package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const IN_MEMORY_MARKER = "IN_MEMORY_MARKER"

type IterModule struct {
	Index    int
	Name     string
	Sections []IterSection
}

type IterSection struct {
	Index    int
	Name     string
	Dir      string
	Lectures []IterLecture
}

type IterLecture struct {
	Index     int
	Name      string
	Lecture   interface{} // Replace with the appropriate type
	Resources []IterResource
}

type IterResource struct {
	Fmt   string
	URL   string
	Title string
}

type CourseDownloader interface {
	DownloadModules(modules []IterModule) bool
}

type CourseraDownloader struct {
	Downloader         Downloader // Replace with the appropriate type
	CommandlineArgs    CommandlineArgs
	ClassName          string
	Path               string
	IgnoredFormats     []string
	DisableURLSkipping bool
	SkippedURLs        []string
	FailedURLs         []string
}

type Downloader interface {
	Download(callback func(string, interface{}), url, lectureFilename string, resume bool)
	Join()
}

type CommandlineArgs struct {
	FileFormats                 []string
	LectureFilter               string
	ResourceFilter              string
	SectionFilter               string
	VerboseDirs                 bool
	CombinedSectionLecturesNums bool
	Playlist                    bool
	Hooks                       []string
	Overwrite                   bool
	Resume                      bool
	SkipDownload                bool
}

func (c *CourseraDownloader) DownloadModules(modules []IterModule) bool {
	completed := true

	for _, module := range modules {
		lastUpdate := int64(-1)

		for _, section := range module.Sections {
			if _, err := os.Stat(section.Dir); os.IsNotExist(err) {
				MkdirP(NormalizePath(section.Dir), os.ModePerm)
			}

			for _, lecture := range section.Lectures {
				for _, resource := range lecture.Resources {
					lectureFilename := NormalizePath(GetLectureFilename(CombinedSectionLecturesNums, section.Dir, section.Index, lecture.Index, lecture.Name, resource.Title, resource.Fmt))
					lastUpdate = c.handleResource(resource.URL, resource.Fmt, lectureFilename, c.downloadCompletionHandler, lastUpdate)
				}
				if CommandlineArgs.Playlist {
					CreateM3UPlaylist(section.Dir)
				}
				if CommandlineArgs.Hooks != nil {
					c.runHooks(section, CommandlineArgs.Hooks)
				}
			}

			completed = completed && IsCourseComplete(lastUpdate)
		}
	}

	c.Downloader.Join()

	return completed
}

func (c *CourseraDownloader) downloadCompletionHandler(url string, result interface{}) {
	if err, ok := result.(error); ok {
		log.Printf("The following error has occurred while downloading URL %s: %s", url, err)
		c.FailedURLs = append(c.FailedURLs, url)
	} else if result != nil {
		log.Printf("Unknown exception occurred: %v", result)
		c.FailedURLs = append(c.FailedURLs, url)
	}
}

func (c *CourseraDownloader) handleResource(url, fmt, lectureFilename string, callback func(string, interface{}), lastUpdate int64) int64 {
	overwrite := CommandlineArgs.Overwrite
	resume := CommandlineArgs.Resume
	skipDownload := CommandlineArgs.SkipDownload

	if overwrite || !FileExists(lectureFilename) || resume {
		if !skipDownload {
			if strings.HasPrefix(url, IN_MEMORY_MARKER) {
				pageContent := url[len(IN_MEMORY_MARKER):]
				log.Printf("Saving page contents to: %s", lectureFilename)
				WriteToFile(lectureFilename, []byte(pageContent))
			} else {
				if c.SkippedURLs != nil && skipFormatURL(fmt, url) {
					c.SkippedURLs = append(c.SkippedURLs, url)
				} else {
					log.Printf("Downloading: %s", lectureFilename)
					c.Downloader.Download(callback, url, lectureFilename, resume)
				}
			}
		} else {
			WriteToFile(lectureFilename, []byte{}) // touch
		}
		lastUpdate = time.Now().Unix()
	} else {
		log.Printf("%s already downloaded", lectureFilename)
		lastUpdate = MaxInt64(lastUpdate, GetFileModTime(lectureFilename).Unix())
	}
	return lastUpdate
}

func (c *CourseraDownloader) runHooks(section IterSection, hooks []string) {
	originalDir, _ := os.Getwd()
	for _, hook := range hooks {
		log.Printf("Running hook %s for section %s.", hook, section.Dir)
		os.Chdir(section.Dir)
		RunCommand(hook)
	}
	os.Chdir(originalDir)
}

func _iterModules(modules []interface{}, className, path string, ignoredFormats []string, args CommandlineArgs) []IterModule {
	var result []IterModule

	for index, module := range modules {
		moduleMap, ok := module.(map[string]interface{})
		if !ok {
			continue
		}

		moduleName, ok := moduleMap["name"].(string)
		if !ok {
			continue
		}

		sections, ok := moduleMap["sections"].([]interface{})
		if !ok {
			continue
		}

		var iterSections []IterSection
		for secNum, section := range sections {
			sectionMap, ok := section.(map[string]interface{})
			if !ok {
				continue
			}

			sectionName, ok := sectionMap["name"].(string)
			if !ok {
				continue
			}

			sectionDir := filepath.Join(path, className, fmt.Sprintf("%02d_%s", secNum+1, moduleName), FormatSection(secNum+1, sectionName, className, args.VerboseDirs))

			lectures, ok := sectionMap["lectures"].([]interface{})
			if !ok {
				continue
			}

			var iterLectures []IterLecture
			for lecNum, lecture := range lectures {
				lectureMap, ok := lecture.(map[string]interface{})
				if !ok {
					continue
				}

				lectureName, ok := lectureMap["name"].(string)
				if !ok {
					continue
				}

				var resources []IterResource
				lectureResources, ok := lectureMap["resources"].([]interface{})
				if ok {
					for _, resource := range lectureResources {
						resourceMap, ok := resource.(map[string]interface{})
						if !ok {
							continue
						}

						resourceFmt, ok := resourceMap["fmt"].(string)

					}
				}
			}
		}
	}
}
