package coursera

import (
	"log"
	"regexp"
	"strings"
)

// ValidFormats is a regex pattern for trusted formats.
const ValidFormats = `^mp4$|^pdf$|^.?.?\.?txt$|^.?.?\.?srt$|.*txt$|.*srt$|^html?$|^zip$|^rar$|^[ct]sv$|^xlsx$|^ipynb$|^json$|^pptx?$|^docx?$|^xls$|^py$|^Rmd$|^Rdata$|^wf1$`

// NonSimpleFormat is a regex pattern for non-simple formats.
const NonSimpleFormat = `.*[^a-zA-Z0-9_-]`

var reValidFormats = regexp.MustCompile(ValidFormats)
var reNonSimpleFormat = regexp.MustCompile(NonSimpleFormat)

// SkipFormatURL checks whether a given format/url should be skipped and not downloaded.
func SkipFormatURL(format, url string) bool {
	// Do not download empty formats
	if format == "" {
		return true
	}

	// Do not download email addresses
	if strings.Contains(url, "mailto:") && strings.Contains(url, "@") {
		return true
	}

	// Is this localhost?
	parsed, err := url.Parse(url)
	if err != nil {
		log.Printf("Error parsing URL: %v", err)
		return true
	}
	if parsed.Hostname() == "localhost" {
		return true
	}

	// These are trusted manually added formats, do not skip them
	if reValidFormats.MatchString(format) {
		return false
	}

	// Simple formats only contain letters, numbers, "_", and "-"
	// If this is a non-simple format?
	if reNonSimpleFormat.MatchString(format) {
		return true
	}

	// Is this a link to the site root?
	if parsed.Path == "" || parsed.Path == "/" {
		return true
	}

	// Do not skip
	return false
}

// FindResourcesToGet selects formats to download.
func FindResourcesToGet(lecture map[string][]Link, fileFormats []string, resourceFilter string, ignoredFormats []string) []Link {
	resourcesToGet := make([]Link, 0)

	if len(ignoredFormats) > 0 {
		log.Printf("The following file formats will be ignored: %s", strings.Join(ignoredFormats, ","))
	}

	for format, resources := range lecture {
		if containsString(ignoredFormats, format) {
			continue
		}

		if containsString(fileFormats, format) || containsString(fileFormats, format) || containsString(fileFormats, "all") {
			for _, r := range resources {
				if resourceFilter != "" && !regexpContains(resourceFilter, r.URL) {
					log.Printf("Skipping because of resource filter: %s %s", resourceFilter, r.URL)
					continue
				}
				resourcesToGet = append(resourcesToGet, r)
			}
		} else {
			log.Printf("Skipping because format %s not in %v", format, fileFormats)
		}
	}

	return resourcesToGet
}

// Link represents a downloadable link.
type Link struct {
	Format string
	Name   string
	URL    string
}

// containsString checks if a string is present in a slice of strings.
func containsString(slice []string, s string) bool {
	for _, element := range slice {
		if element == s {
			return true
		}
	}
	return false
}

// regexpContains checks if a regex pattern is present in a string.
func regexpContains(pattern, s string) bool {
	match, err := regexp.MatchString(pattern, s)
	if err != nil {
		log.Printf("Error matching regex pattern: %v", err)
		return false
	}
	return match
}
