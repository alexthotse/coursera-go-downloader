package coursera

import (
	"fmt"
	"path/filepath"
	"strings"
)

const (
	formatMaxLen = 255 // Replace with your max length
	titleMaxLen  = 255 // Replace with your max length
)

func formatSection(num int, section, className string, verboseDirs bool) string {
	sec := fmt.Sprintf("%02d_%s", num, section)
	if verboseDirs {
		sec = strings.ToUpper(className) + "_" + sec
	}
	return sec
}

func formatResource(num int, name, title, fmt string) string {
	if title != "" {
		title = "_" + title
	}
	return fmt.Sprintf("%02d_%s%s.%s", num, name, title, fmt)
}

func formatCombineNumberResource(secNum, lecNum int, lecName, title, fmt string) string {
	if title != "" {
		title = "_" + title
	}
	return fmt.Sprintf("%02d_%02d_%s%s.%s", secNum, lecNum, lecName, title, fmt)
}

func getLectureFilename(combinedSectionLecturesNums bool, sectionDir string, secNum, lecNum int, lecName, title, fmt string) string {
	// Trim format and title to max length
	fmt = trimToMaxLength(fmt, formatMaxLen)
	title = trimToMaxLength(title, titleMaxLen)

	// Format lecture file name
	var lectureFilename string
	if combinedSectionLecturesNums {
		lectureFilename = filepath.Join(sectionDir, formatCombineNumberResource(secNum+1, lecNum+1, lecName, title, fmt))
	} else {
		lectureFilename = filepath.Join(sectionDir, formatResource(lecNum+1, lecName, title, fmt))
	}

	return lectureFilename
}

// trimToMaxLength trims the input string to the specified max length.
func trimToMaxLength(s string, maxLength int) string {
	if len(s) > maxLength {
		return s[:maxLength]
	}
	return s
}
