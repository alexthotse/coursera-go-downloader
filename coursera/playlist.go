package main

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

func createM3UPlaylist(sectionDir string) {
	pathToReturn, err := os.Getwd()
	if err != nil {
		log.Fatalf("Error getting current working directory: %s", err)
	}

	err = filepath.Walk(sectionDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("Error accessing path %s: %s", path, err)
			return nil
		}

		if info.IsDir() {
			os.Chdir(path)
			globbedVideos, _ := filepath.Glob("*.mp4")
			sort.Strings(globbedVideos)
			m3uName := filepath.Base(path) + ".m3u"

			if len(globbedVideos) > 0 {
				m3uPath := filepath.Join(path, m3uName)
				if err := writeM3UPlaylist(m3uPath, globbedVideos); err != nil {
					log.Printf("Error creating M3U playlist %s: %s", m3uPath, err)
				}
			}

			os.Chdir(pathToReturn)
		}

		return nil
	})

	if err != nil {
		log.Fatalf("Error walking through directory: %s", err)
	}

	os.Chdir(pathToReturn)
}

func writeM3UPlaylist(m3uPath string, videos []string) error {
	content := strings.Join(videos, "\n") + "\n"
	return ioutil.WriteFile(m3uPath, []byte(content), os.ModePerm)
}

func main() {
	// Example usage:
	// createM3UPlaylist("/path/to/your/section/dir")
}
